// Invoke strict mode.
"use strict";

// IMDB API.

// 1. 'requestOptions': sets the method and redirect.
let requestOptions = {
    method: 'GET',
    redirect: 'follow'
  };
// 2. 'fetch': sets the URL and takes in 'requestOptions'.
fetch('https://imdb-api.com/en/API/Title/k_1234567/tt1832382', requestOptions)
    .then(response => response.text())
    .then(result => console.log(result))
    .catch(error => console.log('error', error));