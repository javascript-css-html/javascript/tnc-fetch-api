# tmc-fetch-api

## Description
Following along YouTube video tutorial 'Fetch API Introduction' by Brad Traversy of Traversy Media.

## Badges
![13.4% HTML](https://img.shields.io/static/v1?label=HTML&message=13.4%&color=orange)
![47.6% CSS](https://img.shields.io/static/v1?label=CSS&message=47.6%&color=blue)
![17% JS](https://img.shields.io/static/v1?label=JS&message=17%&color=yellow)
![22% Fetch](https://img.shields.io/static/v1?label=Fetch&message=22%&color=red)

## Installation
This is for practice so I'm installing json package files via NPM(Node Package Manager) with no dependencies so as to activate the snyk VS Code extension. NPM can be installed via a download from https://nodejs.org/en or as I have installed it using NVM(Node Version Manager).

To copy my git starter files /css/styles.css + README.md + .gitignore run these 2 statements in the command line (you will need to install the wget module which I did via Homebrew):

> mkdir css 

> wget https://www.dropbox.com/s/xz3m74rk9kblv7o/styles-setup.css 

> mv styles-setup.css css/styles.css 

> wget -O README.md https://www.dropbox.com/s/8d05riwy7mb062p/READMETUT.md 

> wget https://www.dropbox.com/s/6l7rl93t4r5fvo5/.gitignore 

I started a local instance of git and pushed it to an empty repo on GitHub with the following commands:

> git remote add origin remote_url

> git push -u origin main

## Usage
Strictly for practice.

## Support
GitHub + GitLab + BitBucket: BigSteveLittle

Email: bigsteve@redandblackzone.com

## Authors and acknowledgment
Video Tutorial: Fetch API Introduction
YouTube Channel: https://www.youtube.com/c/TraversyMedia
Video Link: https://youtu.be/Oive66jrwBs

## Project status
![80% Complete](https://img.shields.io/static/v1?label=Completed&message=80%&color=green)
Redoing this prior to doing more react work as it gives a good understanding of APIs with vanilla JS.