// Invoke strict mode.
"use strict";

// Fetch API Sandbox.

// 1. 'get-text': listen for button click.
document.getElementById('get-text').addEventListener('click', getText);
// 2. 'get-films': listen for button click.
document.getElementById('get-films').addEventListener('click', getFilms);
// 3. 'get-albums': listen for button click.
document.getElementById('get-albums').addEventListener('click', getAlbums);
// 4. 'get-films': listen for button click.
document.getElementById('add-album').addEventListener('submit', addAlbum);
// 1a. 'getText': function will get the contents of a text file.
function getText() {
    // 1b. ES5 use of functions.
    fetch('./resources/sample.txt')
        .then(function (response) {
            return response.text()
        })
        .then(function (data) {
            console.log(data)
        })
        .catch((error) => console.log(error))
    // 1c. ES6 use of functions.
    fetch('./resources/sample.txt')
        .then((response) => response.text())
        .then((data) => {
            document.getElementById(
                'text-output'
            ).innerHTML = `<hr><hr>${data}<hr><hr>`;
        })
        .catch((error) => console.log(error));
}
// 2a. 'getFilms': function will get the contents of a json file.
function getFilms() {
    // 2b. ES6 use of functions.
    fetch('./resources/films.json')
        .then((response) => response.json())
        .then((data) => {
            let output = '<h2 class="mb-4">Films</h2>'
            data.forEach(function (film) {
                output += `<div class="list-group mb-3">
                                <p class="list-group-item">${film.releaseYear}</p>
                                <h3 class="list-group-item">${film.releaseTitle}</h3>
                                <p class="list-group-item">${film.plotOutline}</p>
                            </div>`;
                document.getElementById('film-output').innerHTML = output;
            });
        })
        .catch((error) => console.log(error));
}
// 3a. 'getAlbums': function will get the contents from the API
function getAlbums() {
    // 3b. ES6 use of functions.
    fetch('http://jsonplaceholder.typicode.com/albums')
    .then((response) => response.json())
    .then((data) => {
        let output = '<h2 class="mb-4">Albums</h2>';
        data.forEach(function (album) {
            output += `<div class="card card-body mb-3">
                            <p>${album.userId}</p>
                            <h3>${album.title}</h3>
                            <p>${album.id}</p>
                        </div>`;
            document.getElementById('album-output').innerHTML = output;
        });
    })
    .catch((error) => console.log(error));
}
// 4a. 'addAlbum': function will add Album 101.
function addAlbum(event) {
    // form submitting to a file.
    event.preventDefault();
    let userId = document.getElementById('input-user-id').value;
    let title = document.getElementById('input-title').value;
    // 4b. ES6 use of functions.
    fetch('http://jsonplaceholder.typicode.com/albums', {
        method: 'POST',
        headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-type': 'application/json'
        },
        body: JSON.stringify({ userId: userId, title: title })
    })
    .then((response) => response.json())
    .then((data) => console.log(data))
    .catch((error) => console.log(error))
}
